# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Some DirectShow filters for capturing video frames from IP camera, decoding by means of Nvidia display card (invoke its decoder API) and doing camera color calibration by using linear regression algorithm.

* RTSP source filter for parsing the video frames using Live555.
* Cuda h264 decoder for decoding the video and the result will passthrough to the NV12toARGB filter for color transformation directly.
* Video processor will do the calibration by using linear regression algorithm.
* IpCamGraph is a COM object that will provide APIs for application layer to invoke the functions of filters.

### How do I get set up? ###

Use Visual Studio 2010+ for building those projects. You have to build your own Live555 library to make it works (live.lib in this project).
You have to register those filters(.ax file) and IpCamGraph.dll on your platform (Windows only).
And you also need to write your own application to integrate the filters or you can just use GraphEdit which is provided by Microsoft to achieve what you want.